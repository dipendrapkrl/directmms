package umur.karabulut.directmms;

import android.os.Environment;

public class Globals {

	public static final String TAG = "DirectMMS";
	public static String thisDeviceAddress;
	public static String thisDeviceIP;
	public static String justSelectedDeviceAddress;
	public static String fileSavingLocation = Environment
			.getExternalStorageDirectory() + "/DirectMms/Received/";
	public static boolean isActivityAlive = true;
	public static String connectedDeviceIPAddress="";
	public static boolean isInitiator = false;

}

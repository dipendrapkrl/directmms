package umur.karabulut.directmms;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.text.DecimalFormat;

import umur.karabulut.directmms.DetailFragment.Notifier;
import umur.karabulut.directmms.DetailFragment.ProgressBehaviourListener;
import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.format.Time;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.android.internal.telephony.ITelephony;
import com.moysa.mms.mmssender.MMSSender;

public class DirectService extends Service {

	private static final String TAG = Globals.TAG + "DirectService";

	public static final String INTENT_PROGRESS_NOTIFY = "intent_progress_notify";

	public static final String INTENT_FILE_PATH = "fileAbsoulutePath";

	public static final String INTENT_ERROR_IO_EXCEPTION = "IOExceptionOccurred";

	public static final String INTENT_START_SERVER = "startServerObject";

	public static final String INTENT_START_CLIENT = "clientSocket";

	public static final String INTENT_SHOW_DIALOG = "showProgressDialog";

	private static final int SOCKET_TIMEOUT = 100000;
	private static final int SOCKET_TIMEOUT_FOR_META = 5000;
	public static final String EXTRAS_FILE_PATH = "file_url";
	public static final String EXTRAS_ADDRESS = "go_host";
	public static final String EXTRAS_PORT = "go_port";
	public static final String FILE_NAME = "Directmms";
	public static final String FILE_TYPE = "jpg";
	public static final String FILE_MIME_TYPE = "image/jpeg";
	public static final String FILE_SIZE = "filesize";
	public static final String EXTRAS_PHONE_NUMBER = "PHONE_NUMBER";
	public static final String EXTRAS_MESSAGE = "MESSAGE";
	public static final String INTENT_PROGRESS = "PROGESS_VALUE";
	public static final String INTENT_SEND_TO_CLIENT = "sendToClient";
	public static final String INTENT_SEND_TO_SERVER = "sendToServer";
	public static final String INTENT_SEND_TO_SERVER_META = "sendToServerMeta";
	public static final String INTENT_EXTRAS_MAC_ADDRESS = "mac_address";
	public static final String INTENT_EXTRAS_IP_ADDRESS = "ip_address";
	public static final String INTENT_DISMISS_DIALOG = "DISMISS_DIALOG";

	NotificationManager notificationManager;
	NotificationCompat.Builder mBuilder;
	boolean serverRunning = false;
	boolean clientMetaRunning = false;
	boolean clientRunning = false;
	boolean isTransferOngoing = false;
	public static final String INTENT_ADD_BLOCKING_FEATURE = "block_incoming_call";

	protected static final Object INTENT_REMOVE_BLOCKING_FEATURE = "DO_NOT_BLOCK_CALL";

	public static final int TCP_BUFFER_SIZE = 1024 * 1024;

	// private String phoneNumber = "";
	private TelephonyManager telephony;
	private IntentFilter intentFilterGlobal;
	private IntentFilter intentFilterLocal;

	private boolean shouldDropCall = false;

	BroadcastReceiver callReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equalsIgnoreCase(
					"android.intent.action.PHONE_STATE")) {

			} else if (intent.getAction().equals(
					RecordingActivity.INTENT_RECORDED_AUDIO_PATH)) {

				String audioPath = intent
						.getStringExtra(RecordingActivity.INTENT_EXTRAS_AUDIO_LOCATION);
				String message = intent
						.getStringExtra(RecordingActivity.INTENT_EXTRAS_MESSAGE);
				String phoneNumber = intent
						.getStringExtra(RecordingActivity.INTENT_EXTRAS_PHONE_NUMBER);
				File f = new File(audioPath);
				String fileExtension = MimeTypeMap
						.getFileExtensionFromUrl(audioPath);
				String fileMimeType = MimeTypeMap.getSingleton()
						.getMimeTypeFromExtension(fileExtension);

				if (fileMimeType == null)
					fileMimeType = "*/*";
				String ip = new Preferences(DirectService.this)
						.getIPFromMac(Globals.justSelectedDeviceAddress);

				Intent intent1 = new Intent();
				intent1.putExtra(EXTRAS_FILE_PATH, audioPath);
				intent1.putExtra(EXTRAS_ADDRESS,
						Globals.connectedDeviceIPAddress);
				intent1.putExtra(FILE_NAME, f.getName());
				intent1.putExtra(FILE_MIME_TYPE, fileMimeType);
				intent1.putExtra(FILE_SIZE, f.length());
				intent1.putExtra(EXTRAS_PHONE_NUMBER, phoneNumber);
				intent1.putExtra(EXTRAS_MESSAGE, message);
				intent1.putExtra(EXTRAS_PORT, DetailFragment.PORT);

				Log.v(TAG, "client is started" + Utility.getIPAddress());// Utility.getLocalIpv4Address());
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
					new AsyncClient().executeOnExecutor(
							AsyncTask.THREAD_POOL_EXECUTOR, (intent1));
				else
					new AsyncClient().execute(intent1);

				// Now, send the file to the next device okay!

			} else if (intent.getAction().equals(INTENT_ADD_BLOCKING_FEATURE)) {
				// This intent should be fired before enabling call blocking
				// feature
				addBlockingFeature(true);
			} else if (intent.getAction()
					.equals(INTENT_REMOVE_BLOCKING_FEATURE)) {
				addBlockingFeature(false);
			} else if (intent.getAction().equals("emulatePhoneCall")) {
				Toast.makeText(getApplicationContext(),
						"Assuming fake call from 1234567890 ",
						Toast.LENGTH_SHORT).show();
				dropCall("1234567890", true);
			}
		}
	};

	private BroadcastReceiver receiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {

			if (intent.getAction().equals(INTENT_START_SERVER)) {
				if (!serverRunning) {

					Log.v(TAG, "server started at " + Utility.getIPAddress());// Utility.getLocalIpv4Address());
					// Utility.toast(getApplicationContext(),
					// "Server Started ");

					// Utility.getLocalIpv4Address());
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
						new AsyncServer().executeOnExecutor(
								AsyncTask.THREAD_POOL_EXECUTOR, (Void[]) null);
					else
						new AsyncServer().execute((Void[]) null);
				} else {
					// Utility.toast(getApplicationContext(),
					// "already running");
					Log.v(TAG, "Server is already running");
				}
			} else if (intent.getAction().equals(INTENT_START_CLIENT)) {
				// Utility.toast(getApplicationContext(), "Client Started");

				Log.v(TAG, "client is started" + Utility.getIPAddress());// Utility.getLocalIpv4Address());
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
					new AsyncClient().executeOnExecutor(
							AsyncTask.THREAD_POOL_EXECUTOR, (intent));
				else
					new AsyncClient().execute(intent);

			} else if (intent.getAction().equals(INTENT_SEND_TO_SERVER_META)) {
				if (!clientMetaRunning) {
					// Toast.makeText(ClientService.this,
					// "start client meta",Toast.LENGTH_LONG).show();
					Log.v(TAG, "trying to send meta info");
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
						new AsyncClientMeta().executeOnExecutor(
								AsyncTask.THREAD_POOL_EXECUTOR, (intent));
					else
						new AsyncClientMeta().execute(intent);
				}

			}

		}
	};

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		Log.v(TAG, "service created");

		registerLocalBroadcast();
		notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		mBuilder = new Builder(this);

		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(INTENT_START_CLIENT);
		intentFilter.addAction(INTENT_START_SERVER);
		intentFilter.addAction(INTENT_SEND_TO_SERVER_META);
		LocalBroadcastManager.getInstance(this).registerReceiver(receiver,
				intentFilter);
		/*
		 * if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) new
		 * AsyncCheck().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR); else
		 * new AsyncCheck().execute();
		 */

	}

	private void registerLocalBroadcast() {
		setLocalIntentFilterActions();
		LocalBroadcastManager.getInstance(this).registerReceiver(callReceiver,
				intentFilterLocal);
	}

	private void addBlockingFeature(boolean shouldActivateBlocking) {
		if (shouldActivateBlocking) {
			shouldDropCall = true;
			telephony = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
			telephony.listen(new MyPhoneStateListener(),
					PhoneStateListener.LISTEN_CALL_STATE);
			setGlobalIntentFilterActions();
			registerReceiver(callReceiver, intentFilterGlobal);
		} else {
			shouldDropCall = false;
			telephony = null;
			unregisterReceiver(callReceiver);
		}

	}

	private void setGlobalIntentFilterActions() {
		intentFilterGlobal = new IntentFilter();
		intentFilterGlobal.addAction("android.intent.action.PHONE_STATE");
	}

	private void setLocalIntentFilterActions() {
		intentFilterLocal = new IntentFilter();
		intentFilterLocal.addAction(INTENT_ADD_BLOCKING_FEATURE);
		intentFilterLocal.addAction("emulatePhoneCall");
		intentFilterLocal
				.addAction(RecordingActivity.INTENT_RECORDED_AUDIO_PATH);

	}

	private void dropCall(String phoneNumber, boolean isTesting) {

		if (!shouldDropCall) {
			Log.v(TAG, "shouldnot drop call, returning");
			if (isTesting)
				Utility.toast(this,
						"Call will not be dropped. Re-connect if it is abnormal");
			return;
		}

		if (phoneNumber == null || phoneNumber.equalsIgnoreCase("")) {
			Toast.makeText(
					this,
					"Phone Number Couldnt be retrieved,  Not dropping the call",
					Toast.LENGTH_LONG).show();
			return;
		}
		if (isTesting)
			Utility.toast(this,
					"Call will be dropped. Re-connect if it is abnormal");
		Log.v(TAG, "should drop call");
		try {

			Class c = Class.forName(telephony.getClass().getName());
			Method m = c.getDeclaredMethod("getITelephony");
			m.setAccessible(true);
			ITelephony telephonyService = (ITelephony) m.invoke(telephony);
			// telephonyService.silenceRinger();
			// telephonyService.endCall();
			Toast.makeText(this, phoneNumber, Toast.LENGTH_LONG).show();
			sendPhoneNumberToNextDevice(phoneNumber);
		} catch (Exception e) {
			e.printStackTrace();
			Log.v(TAG, "failed to drop call, something went wrong");
		}
	}

	private void sendPhoneNumberToNextDevice(String phoneNumber) {
		Log.v(TAG, "send phone number to next device");
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
			new AsyncClientPhone().executeOnExecutor(
					AsyncTask.THREAD_POOL_EXECUTOR, (phoneNumber));
		else
			new AsyncClientPhone().execute(phoneNumber);

	}

	private void startRecorderActivity(String phoneNumber) {
		Intent intent = new Intent(getBaseContext(), RecordingActivity.class);
		intent.putExtra("phoneNumber", phoneNumber);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		getApplication().startActivity(intent);

	}

	public class MyPhoneStateListener extends PhoneStateListener {
		@Override
		public void onCallStateChanged(int state, String incomingNumber) {
			switch (state) {
			case TelephonyManager.CALL_STATE_IDLE:
				Log.d("DEBUG", "IDLE");
				break;
			case TelephonyManager.CALL_STATE_OFFHOOK:
				Log.d("DEBUG", "OFFHOOK");
				break;
			case TelephonyManager.CALL_STATE_RINGING:
				Log.d("DEBUG", "RINGING");
				dropCall(incomingNumber, false);

				break;
			}
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
		if (shouldDropCall)
			unregisterReceiver(callReceiver);
		LocalBroadcastManager.getInstance(this)
				.unregisterReceiver(callReceiver);
	}

	private class AsyncClient extends AsyncTask<Intent, Void, Void> {
		double percent;
		boolean isCompleted, isDownloading;
		float speedMbps;
		String timeRemaining;
		String fileName;
		boolean isSuccessful = false;

		@Override
		protected Void doInBackground(Intent... intent) {
			Log.v(TAG, "in background");
			clientTask(intent[0]);
			return null;
		}

		private void clientTask(Intent intent) {
			Log.v(TAG, "Client is running");

			// Context context = getApplicationContext();
			String fileUri = intent.getExtras().getString(EXTRAS_FILE_PATH);
			String host = intent.getExtras().getString(EXTRAS_ADDRESS);
			String fileName = intent.getExtras().getString(FILE_NAME);
			String fileMimeType = intent.getExtras().getString(FILE_MIME_TYPE);
			final long sizeInBytes = intent.getExtras().getLong(FILE_SIZE);
			String phoneNumber = intent.getExtras().getString(
					EXTRAS_PHONE_NUMBER);
			String message = intent.getExtras().getString(EXTRAS_MESSAGE);
			Socket socket = new Socket();
			int port = intent.getExtras().getInt(EXTRAS_PORT);

			try {

				Log.d(TAG, "Opening client socket - host= " + host
						+ " and port = " + port);
				socket.setSendBufferSize(TCP_BUFFER_SIZE);
				socket.bind(null);
				socket.connect((new InetSocketAddress(host, port)),
						SOCKET_TIMEOUT);

				Log.d(TAG, "Client socket - " + socket.isConnected());
				OutputStream stream = new BufferedOutputStream(
						socket.getOutputStream());
				ContentResolver cr = DirectService.this.getApplicationContext()
						.getContentResolver();
				InputStream is = null;

				BufferedWriter bufferStream = new BufferedWriter(
						new OutputStreamWriter(stream));
				Log.d(TAG, "Socket write----------- name: " + fileName);

				bufferStream.write("not-meta");
				bufferStream.newLine();
				bufferStream.write(fileName);
				bufferStream.newLine();
				bufferStream.write(String.valueOf(sizeInBytes));
				bufferStream.newLine();
				Log.d(TAG, "Socket write----------- mime: " + fileMimeType);
				bufferStream.write(fileMimeType);
				bufferStream.newLine();
				bufferStream.write(phoneNumber);
				bufferStream.newLine();
				bufferStream.write(message);
				bufferStream.newLine();
				bufferStream.flush();

				Log.d(TAG, "Socket write----------- file");

				try {
					// is = new
					// BufferedInputStream(cr.openInputStream(Uri.parse(fileUri)),
					// 8 * 1024);
					is = new BufferedInputStream(new FileInputStream(new File(
							fileUri)));
					// is = new
					// BufferedInputStream(cr.openInputStream(Uri.parse(fileUri)));

				} catch (FileNotFoundException e) {

					Log.d(TAG, e.toString());
					Log.v(TAG, "File not found");
					isSuccessful = false;

				}

				copyFile(is, stream, sizeInBytes, false, fileName,
						new Notifier() {

							@Override
							public void notifier(double percent,
									String fileName, boolean isCompleted,
									boolean isDownloading, float speedMbps,
									String timeRemaining) {
								AsyncClient.this.percent = percent;
								AsyncClient.this.fileName = fileName;
								AsyncClient.this.isCompleted = isCompleted;
								AsyncClient.this.isDownloading = isDownloading;
								AsyncClient.this.speedMbps = speedMbps;
								AsyncClient.this.timeRemaining = timeRemaining;

								onProgressUpdate();

							}
						}, null);

				// Log.d(TAG, "sent stream is= "+stream.toString());

				Log.d(TAG, "Client: Data written");
				isSuccessful = true;

			} catch (IOException e) {
				Log.e(TAG, e.getMessage());
				isSuccessful = false;
			} finally {
				if (socket != null) {
					if (socket.isConnected()) {
						try {
							socket.close();
						} catch (IOException e) {
							// Give up
							e.printStackTrace();
						}
					}
				}
			}

		}

		@Override
		protected void onProgressUpdate(Void... values) {

			// TODO Auto-generated method stub
			Intent intent = new Intent(INTENT_PROGRESS_NOTIFY);
			intent.putExtra("percent", percent);
			intent.putExtra("fileName", fileName);
			intent.putExtra("isCompleted", isCompleted);
			intent.putExtra("isDownloading", isDownloading);
			intent.putExtra("speedMbps", speedMbps);
			intent.putExtra("timeRemaining", timeRemaining);
			intent.setClass(getApplicationContext(), HomeActivity.class);
			LocalBroadcastManager.getInstance(getApplicationContext())
					.sendBroadcast(intent);
			// showNotification(notificationManager, mBuilder, percent,
			// fileName,isCompleted, isDownloading, speedMbps, timeRemaining);

		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			Intent intent = new Intent(INTENT_DISMISS_DIALOG);
			intent.setClass(DirectService.this, HomeActivity.class);
			LocalBroadcastManager.getInstance(DirectService.this)
					.sendBroadcast(intent);
			Log.v(TAG, "try to dismis dialog");
			if (!isSuccessful)
				Utility.toast(DirectService.this, "Sorry, Please send again ");
		}

	}

	private class AsyncClientPhone extends AsyncTask<String, Void, Void> {

		@Override
		protected Void doInBackground(String... string) {
			Log.v(TAG, "client meta : in background");
			// clientMetaRunning = true;
			while (true) {
				boolean isSuccessful = clientTask(string[0]);
				if (isSuccessful)
					break;
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Log.v(TAG, "Failed to send Meta, Sending again");
			}
			// clientMetaRunning = false;
			Log.v(TAG, "Sent infomation to server");
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			Toast.makeText(DirectService.this, "Phone Number sent",
					Toast.LENGTH_LONG).show();
		}

		private boolean clientTask(String phoneNumber) {
			boolean succeed = false;
			Log.v(TAG, "Client is running");

			Socket socket = new Socket();

			try {

				Log.d(TAG, "Opening phone client socket - ");
				socket.setReceiveBufferSize(TCP_BUFFER_SIZE);
				socket.bind(null);
				socket.connect(
						(new InetSocketAddress(
								Globals.connectedDeviceIPAddress,
								DetailFragment.PORT)), SOCKET_TIMEOUT_FOR_META);

				Log.d(TAG, "Client socket - " + socket.isConnected());
				OutputStream stream = new BufferedOutputStream(
						socket.getOutputStream());

				BufferedWriter bufferStream = new BufferedWriter(
						new OutputStreamWriter(stream));

				bufferStream.write("phoneNumber");
				bufferStream.newLine();
				bufferStream.flush();
				bufferStream.write(phoneNumber);
				bufferStream.newLine();
				bufferStream.flush();
				stream.close();

				succeed = true;
				Log.d(TAG, "phone information: Data written");

			} catch (IOException e) {
				Log.e(TAG, e.getMessage());
				succeed = false;

			} finally {
				if (socket != null) {
					if (socket.isConnected()) {
						try {
							socket.close();
						} catch (IOException e) {
							// Give up
							e.printStackTrace();

						}
					}
				}
			}

			return succeed;
		}

	}

	private class AsyncClientMeta extends AsyncTask<Intent, Void, Void> {

		@Override
		protected Void doInBackground(Intent... intent) {
			Log.v(TAG, "client meta : in background");
			clientMetaRunning = true;
			while (true) {
				boolean isSuccessful = clientTask(intent[0]);
				if (isSuccessful)
					break;
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Log.v(TAG, "Failed to send Meta, Sending again");
			}
			clientMetaRunning = false;
			Log.v(TAG, "Sent infomation to server");
			return null;
		}

		private boolean clientTask(Intent intent) {
			boolean succeed = false;
			Log.v(TAG, "Client is running");

			// Context context = getApplicationContext();
			String macAddress = intent.getExtras().getString(
					INTENT_EXTRAS_MAC_ADDRESS);
			String ipAddress = intent.getExtras().getString(
					INTENT_EXTRAS_IP_ADDRESS);
			String host = intent.getExtras().getString(EXTRAS_ADDRESS);
			Socket socket = new Socket();
			try {
				socket.setSendBufferSize(TCP_BUFFER_SIZE);
			} catch (SocketException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			int port = intent.getExtras().getInt(EXTRAS_PORT);

			try {

				Log.d(TAG, "Opening meta client socket - ");
				socket.bind(null);
				socket.connect((new InetSocketAddress(host, port)),
						SOCKET_TIMEOUT_FOR_META);

				Log.d(TAG, "Client socket - " + socket.isConnected());
				OutputStream stream = new BufferedOutputStream(
						socket.getOutputStream());

				BufferedWriter bufferStream = new BufferedWriter(
						new OutputStreamWriter(stream));

				bufferStream.write("meta");
				bufferStream.newLine();
				bufferStream.flush();
				bufferStream.write(ipAddress);
				bufferStream.newLine();
				bufferStream.flush();
				bufferStream.write(macAddress);
				bufferStream.newLine();
				bufferStream.flush();
				bufferStream.write(Globals.isInitiator ? "initiator"
						: "non-initiator");
				bufferStream.newLine();
				bufferStream.flush();
				stream.close();

				succeed = true;
				Log.d(TAG, "meta Client: Data written");

			} catch (IOException e) {
				Log.e(TAG, e.getMessage());
				succeed = false;

			} finally {
				if (socket != null) {
					if (socket.isConnected()) {
						try {
							socket.close();
						} catch (IOException e) {
							// Give up
							e.printStackTrace();

						}
					}
				}
			}

			return succeed;
		}

	}

	private class AsyncServer extends AsyncTask<Void, Void, Void> {
		double percent;
		boolean isCompleted, isDownloading;
		String fileName;
		String fileSizeInBytes;
		String phoneNumber;
		String message;
		float speedMbps;
		String timeRemaining;
		File f;
		String fileMime;
		boolean isSuccessful;
		boolean isMetaConnection = false;
		boolean isPhoneNoReceived = false;
		private String phoneNumberReceived = "";

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			Log.v(TAG, "Server started");

		}

		@Override
		protected Void doInBackground(Void... params) {
			serverTask();
			return null;
		}

		@Override
		protected void onProgressUpdate(Void... values) {
			// TODO Auto-generated method stub

			Intent intent = new Intent(INTENT_PROGRESS_NOTIFY);
			intent.putExtra("percent", percent);
			intent.putExtra("fileName", fileName);
			intent.putExtra("isCompleted", isCompleted);
			intent.putExtra("isDownloading", isDownloading);
			intent.putExtra("speedMbps", speedMbps);
			intent.putExtra("timeRemaining", timeRemaining);
			intent.setClass(getApplicationContext(), HomeActivity.class);
			LocalBroadcastManager.getInstance(getApplicationContext())
					.sendBroadcast(intent);
			// showNotification(notificationManager, mBuilder, percent,
			// fileName, isCompleted, isDownloading, speedMbps, timeRemaining);

		}

		@Override
		protected void onPostExecute(Void result) {
			if (isMetaConnection) {
				Log.v(TAG, "server started at " + Utility.getIPAddress());// Utility.getLocalIpv4Address());

				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
					new AsyncServer().executeOnExecutor(
							AsyncTask.THREAD_POOL_EXECUTOR, (Void[]) null);
				else
					new AsyncServer().execute((Void[]) null);
				return;
			}
			if (isPhoneNoReceived) {
				playNotification();
				startRecorderActivity(phoneNumberReceived);

				return;
			}
			if (isSuccessful) {
				Utility.toast(DirectService.this, "File have been saved to "
						+ Globals.fileSavingLocation);

				MMSSender sender = new MMSSender(DirectService.this);
				try {
					Log.v(TAG, "phone number=" + phoneNumber + " and message="
							+ message);
					sender.sendMMS1(phoneNumber, message, new FileInputStream(
							new File(Globals.fileSavingLocation + fileName)));
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Utility.toast(DirectService.this,
							"Failed to read audio file");
				}

			} else {
				Intent intent = new Intent(INTENT_ERROR_IO_EXCEPTION);
				intent.setClass(getApplicationContext(), HomeActivity.class);
				LocalBroadcastManager.getInstance(getApplicationContext())
						.sendBroadcast(intent);
			}

			// Start server again
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
				new AsyncServer().executeOnExecutor(
						AsyncTask.THREAD_POOL_EXECUTOR, (Void[]) null);
			else
				new AsyncServer().execute((Void[]) null);

		}

		private void playNotification() {
			try {
				Uri notification = RingtoneManager
						.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
				Ringtone r = RingtoneManager.getRingtone(
						getApplicationContext(), notification);
				r.play();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// Start the server when intent is received to do so
		private void serverTask() {
			Log.v(TAG, "server task");
			try {
				serverRunning = true;
				ServerSocket serverSocket = new ServerSocket();
				serverSocket.setReceiveBufferSize(TCP_BUFFER_SIZE);
				InetSocketAddress address = new InetSocketAddress(
						DetailFragment.PORT);
				serverSocket.bind(address);
				Log.d(TAG, "Server: Socket opened");
				Log.d(TAG, "Waiting to receive file");
				Socket client = serverSocket.accept();
				Log.d(TAG, "Server: connection done");

				InputStream inputstream = new BufferedInputStream(
						client.getInputStream());
				// new BufferedInputStream(client.getInputStream(), 8 * 1024);
				BufferedReader bufferedStream = new BufferedReader(
						new InputStreamReader(inputstream));

				// Log.v(TAG, "received "+inputstream.toString());

				String metaInfo = bufferedStream.readLine();
				if (metaInfo.equalsIgnoreCase("meta")) {

					isMetaConnection = true;
					String ipAddress = bufferedStream.readLine();
					String macAddress = bufferedStream.readLine();
					Log.v(TAG, "ipAddress" + ipAddress);
					Log.v(TAG, "mac = " + macAddress);
					Globals.connectedDeviceIPAddress = ipAddress;
					// MacIpPair.addMacIPPair(macAddress, ipAddress);
					new Preferences(DirectService.this).setIPForMac(macAddress,
							ipAddress);
					inputstream.close();

				} else if (metaInfo.equalsIgnoreCase("phoneNumber")) {
					String phoneNumber = bufferedStream.readLine();
					Log.v(TAG, "phone number=" + phoneNumber);
					phoneNumberReceived = phoneNumber;
					inputstream.close();
					isPhoneNoReceived = true;
					// Start Recorder Application

				} else {

					fileName = bufferedStream.readLine();
					fileSizeInBytes = bufferedStream.readLine();
					fileMime = bufferedStream.readLine();
					phoneNumber = bufferedStream.readLine();
					message = bufferedStream.readLine();

					f = new File(Globals.fileSavingLocation + fileName);

					File dirs = new File(f.getParent());
					if (!dirs.exists())
						dirs.mkdirs();

					if (f.exists()) {
						f.delete();
					}

					f.createNewFile();

					Log.d(TAG, "server: copying files " + f.toString());
					Log.v(TAG, "size of file =" + fileSizeInBytes);
					long size = Long.parseLong(fileSizeInBytes);
					Log.d(TAG, "size in long" + size);
					Log.d(TAG, "file mime = " + fileMime);

					copyFile(inputstream, new FileOutputStream(f), size, true,
							fileName, new Notifier() {

								@Override
								public void notifier(double percent,
										String fileName, boolean isCompleted,
										boolean isDownloading, float speedMbps,
										String timeRemaining) {
									AsyncServer.this.percent = percent;
									AsyncServer.this.fileName = fileName;
									AsyncServer.this.isCompleted = isCompleted;
									AsyncServer.this.isDownloading = isDownloading;
									AsyncServer.this.speedMbps = speedMbps;
									AsyncServer.this.timeRemaining = timeRemaining;

									onProgressUpdate();
								}
							}, null);

					serverSocket.close();
				}
				isSuccessful = true;

			} catch (IOException e) {
				isSuccessful = false;
				Log.e(TAG, e.getMessage());

			}
			serverRunning = false;

		}

	}

	/**
	 * 
	 * @param percent
	 * @param fileName
	 * @param isCompleted
	 *            true if transfer completed;
	 * @param isDownloading
	 *            true if downloading, false if uploading;
	 */
	public void showNotification(NotificationManager notificationManager,
			NotificationCompat.Builder mBuilder, double percent,
			String fileName, boolean isCompleted, boolean isDownloading,
			float speedMbps, String remainingTime) {

		String downloadOrUpload;

		if (isDownloading) {
			downloadOrUpload = "Receiving";
		} else {
			downloadOrUpload = "Sending";
		}
		// DecimalFormat df = new DecimalFormat("00.00");
		DecimalFormat df = new DecimalFormat("0.00");
		String time = remainingTime.equalsIgnoreCase("") ? "" : remainingTime
				+ " remaining";

		String content = df.format(percent) + "% @ " + df.format(speedMbps)
				+ "Mbps  " + time;

		if (isCompleted)
			content = "Recorded Audio "
					+ ((isDownloading) ? "Received" : "Sent");

		mBuilder.setContentTitle(downloadOrUpload + " " + fileName)
				.setContentText(content)
				.setSmallIcon(R.drawable.app_icon_notification);
		mBuilder.setProgress(100, (int) percent, false);
		notificationManager.notify(1, mBuilder.build());

	}

	public boolean copyFile(InputStream inputStream, OutputStream out,
			long totalSizeInBytes, boolean isDownloading, String fileName,
			Notifier notifier,
			ProgressBehaviourListener progressBehaviourListener) {

		isTransferOngoing = true;
		final int SIZE = 1024 * 8;
		byte buf[] = new byte[SIZE];
		Integer totalTransferredBytes = 0;

		int len;
		long a = 0;
		float speedMbps = 0;
		try {
			Log.d(TAG, "File write started");
			Time t = new Time();
			t.setToNow();
			long timeMilli = t.toMillis(true);
			notifier.notifier(0.00, fileName, true, isDownloading, speedMbps,
					"");
			if (progressBehaviourListener != null)
				progressBehaviourListener.updateDialog(0.00, fileName, true,
						isDownloading, speedMbps);

			long transferred = 0;
			while ((len = inputStream.read(buf)) != -1) {
				totalTransferredBytes += len;
				transferred += len;
				out.write(buf, 0, len);

				double pern = getPercent(totalTransferredBytes,
						totalSizeInBytes);
				if (a % 100 == 0) {

					Time tNew = new Time();
					tNew.setToNow();
					long currentMilli = tNew.toMillis(true);
					long diffSec = (currentMilli - timeMilli) / 1000;

					if (diffSec >= 1) {
						timeMilli = currentMilli;
						speedMbps = (float) transferred * 8
								/ ((float) diffSec * 1024 * 1024);
						long remainingBytes = (totalSizeInBytes - totalTransferredBytes);
						float remainingMb = (remainingBytes * 8)
								/ (1024 * 1024); // making
						// it
						// Mb
						Log.v(TAG, "remaining=" + remainingMb);

						long remainingSecs = (long) (remainingMb / speedMbps);
						String timeRemaining = getTime(remainingSecs);

						transferred = 0;

						// Log.v(TAG, "speed = " + speedMbps);

						if (notifier != null) {

							if (notifier != null)
								notifier.notifier(pern, fileName, false,
										isDownloading, speedMbps, timeRemaining);
							if (progressBehaviourListener != null)
								progressBehaviourListener.updateDialog(pern,
										fileName, false, isDownloading,
										speedMbps);
						}
					}
				}
				a++;
				buf = new byte[SIZE];

			}
			notifier.notifier(100.00, fileName, true, isDownloading, speedMbps,
					"");
			if (progressBehaviourListener != null)
				progressBehaviourListener.updateDialog(100.00, fileName, true,
						isDownloading, speedMbps);

			out.close();
			inputStream.close();
		} catch (IOException e) {
			Log.d(TAG, e.toString());
			isTransferOngoing = false;
			return false;
		}
		if (progressBehaviourListener != null)
			progressBehaviourListener.dismisDialog();
		isTransferOngoing = false;

		return true;
	}

	private String getTime(long remainingSecs) {
		Log.v(TAG, "remaining sec=" + remainingSecs);
		long hour = 0, min = 0, sec = 0;
		hour = remainingSecs / (60 * 60);
		remainingSecs = remainingSecs % (60 * 60);
		min = remainingSecs / 60;
		remainingSecs = remainingSecs % 60;
		sec = remainingSecs;

		String time = (hour != 0) ? hour + " hr " : "";
		time += (min != 0) ? min + " min " : "";
		time += (sec != 0) ? sec + " sec " : "";
		return time;

	}

	private static float getPercent(long sent, long sizeInBytes) {

		double percent = (double) sent / (double) sizeInBytes;
		percent *= 100;
		return (float) percent;
	}

}

package umur.karabulut.directmms;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class Preferences {

	// private static final String TAG = "Preferences";
	private static final String APP_SHARED_PREFS = "np.pro.dipendra.directmms_preferences";

	private SharedPreferences appSharedPrefs;
	private Editor prefsEditor;

	public Preferences(Context context) {

		appSharedPrefs = context.getSharedPreferences(APP_SHARED_PREFS,
				Context.MODE_PRIVATE);
		prefsEditor = appSharedPrefs.edit();
	}

	public String getIPFromMac(String macAddress) {
		return appSharedPrefs.getString(macAddress, "");
	}

	public void setIPForMac(String macAddress, String ipAddress) {

		prefsEditor.putString(macAddress, ipAddress);
		prefsEditor.commit();

	}

	/**
	 * 
	 * @return return 0 if user is not yet registered
	 */
	public String getStatus() {
		return appSharedPrefs.getString("status", "");
	}

	public void setStatus(String value) {
		prefsEditor.putString("status", value);
		prefsEditor.commit();
	}

}

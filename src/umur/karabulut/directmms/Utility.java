package umur.karabulut.directmms;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.apache.http.conn.util.InetAddressUtils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class Utility {

	private static final String TAG = Globals.TAG + "Utility";

	public static void toast(Context context, String msg) {
		Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
	}

	/**
	 * Returns MAC address of the given interface name.
	 * 
	 * @param interfaceName
	 *            eth0, wlan0 or NULL=use first interface
	 * @return mac address or empty string
	 */
	private static String getMACAddress(String interfaceName) {
		try {
			List<NetworkInterface> interfaces = Collections
					.list(NetworkInterface.getNetworkInterfaces());

			for (NetworkInterface intf : interfaces) {
				if (interfaceName != null) {
					if (!intf.getName().equalsIgnoreCase(interfaceName))
						continue;
				}
				byte[] mac = intf.getHardwareAddress();
				if (mac == null)
					return "";
				StringBuilder buf = new StringBuilder();
				for (int idx = 0; idx < mac.length; idx++)
					buf.append(String.format("%02X:", mac[idx]));
				if (buf.length() > 0)
					buf.deleteCharAt(buf.length() - 1);
				return buf.toString();
			}
		} catch (Exception ex) {
		} // for now eat exceptions
		return "";
		/*
		 * try { // this is so Linux hack return
		 * loadFileAsString("/sys/class/net/" +interfaceName +
		 * "/address").toUpperCase().trim(); } catch (IOException ex) { return
		 * null; }
		 */
	}

	/**
	 * 
	 * @return ip address of the first interface which have "p2p" as characters
	 *         sequence in its interface name and have ip address that starts
	 *         with "192.168.49."
	 */
	public static String getIPAddress() {
		try {
			List<NetworkInterface> interfaces = Collections
					.list(NetworkInterface.getNetworkInterfaces());
			/*
			 * for (NetworkInterface networkInterface : interfaces) { Log.v(TAG,
			 * "interface name " + networkInterface.getName() + "mac = " +
			 * getMACAddress(networkInterface.getName())); }
			 */

			for (NetworkInterface intf : interfaces) {
				if (!getMACAddress(intf.getName()).equalsIgnoreCase(
						Globals.thisDeviceAddress)) {
					// Log.v(TAG, "ignore the interface " + intf.getName());
					// continue;
				}
				if (!intf.getName().contains("p2p"))
					continue;

				Log.v(TAG,
						intf.getName() + "   " + getMACAddress(intf.getName()));

				List<InetAddress> addrs = Collections.list(intf
						.getInetAddresses());

				for (InetAddress addr : addrs) {
					// Log.v(TAG, "inside");

					if (!addr.isLoopbackAddress()) {
						// Log.v(TAG, "isnt loopback");
						String sAddr = addr.getHostAddress().toUpperCase(
								Locale.US);
						Log.v(TAG, "ip=" + sAddr);

						boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);

						if (isIPv4) {
							if (sAddr.contains("192.168.49.")) {
								Log.v(TAG, "ip = " + sAddr);
								return sAddr;
							}
						}

					}

				}
			}

		} catch (Exception ex) {
			Log.v(TAG, "error in parsing");
		} // for now eat exceptions
		Log.v(TAG, "returning null ip address");
		return "";
	}

}

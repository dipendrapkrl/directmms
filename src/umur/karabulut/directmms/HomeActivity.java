package umur.karabulut.directmms;

import org.json.JSONException;
import org.json.JSONObject;

import umur.karabulut.directmms.DetailFragment.Notifier;
import umur.karabulut.directmms.DetailFragment.ProgressBehaviourListener;
import umur.karabulut.directmms.DeviceListFragment.DeviceActionListener;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.ActionListener;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.net.wifi.p2p.WifiP2pManager.ChannelListener;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class HomeActivity extends Activity implements ChannelListener,
		DeviceActionListener, Notifier, ProgressBehaviourListener {

	private static final String TAG = Globals.TAG + "WifiDirectActivity";
	private static final int JELLYBEAN = 16;
	private int discoveryCallCounter = 0;

	private WifiP2pManager manager;
	private boolean isWifiP2pEnabled = false;
	private boolean retryChannel = false;

	private final IntentFilter intentFilter = new IntentFilter();
	private final IntentFilter intentsClientService = new IntentFilter();
	private Channel channel;
	private BroadcastReceiver receiver = null;

	private boolean isDialogShownOnce = false;

	private BroadcastReceiver clientServiceListener = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(DirectService.INTENT_PROGRESS_NOTIFY)) {
				double percent = intent.getDoubleExtra("percent", 0.00);
				String fileName = intent.getStringExtra("fileName");
				boolean isCompleted = intent.getBooleanExtra("isCompleted",
						false);
				boolean isDownloading = intent.getBooleanExtra("isDownloading",
						false);
				float speedMbps = intent.getFloatExtra("speedMbps", 0);

				// notifier(percent, fileName, isCompleted, isDownloading);
				updateDialog(percent, fileName, isCompleted, isDownloading,
						speedMbps);

			} else if (intent.getAction()
					.equals(DirectService.INTENT_FILE_PATH)) {

				Intent intent1 = new Intent();
				intent1.setAction(android.content.Intent.ACTION_VIEW);

				Uri uri = Uri.parse("file://"
						+ intent.getStringExtra("filePath"));
				intent1.setDataAndType(uri, intent.getStringExtra("fileMime"));
				startActivity(intent1);

			} else if (intent.getAction().equals(
					DirectService.INTENT_ERROR_IO_EXCEPTION)) {
				Toast.makeText(HomeActivity.this,
						"Error, Please Try again", Toast.LENGTH_SHORT).show();

			}

		}
	};

	/**
	 * @param isWifiP2pEnabled
	 *            the isWifiP2pEnabled to set
	 */
	public void setIsWifiP2pEnabled(boolean isWifiP2pEnabled) {
		this.isWifiP2pEnabled = isWifiP2pEnabled;

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		ActionBar actionBar = getActionBar();

		// actionBar.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.header));
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.show();

		if (new Preferences(this).getStatus().equalsIgnoreCase("C")) {
			startActivity(new Intent(this, ContactActivity.class));
			stopService(new Intent(this, DirectService.class));
			finish();
		}
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
			new AsyncCheck().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		else
			new AsyncCheck().execute();

		// add necessary intent values to be matched.
		addActionsToIntentFilters();

		turnOnWifiOptional();
		manager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
		channel = manager.initialize(this, getMainLooper(), null);
		Log.v(TAG, "Initialized");

	}

	private void turnOnWifiOptional() {
		if (android.os.Build.VERSION.SDK_INT >= JELLYBEAN) {
			WifiManager manager = (WifiManager) getSystemService(WIFI_SERVICE);

			if (!manager.isWifiEnabled()) {
				manager.setWifiEnabled(true);
				Utility.toast(this, "Turning on WiFi");
			}
		}

	}

	private void addActionsToIntentFilters() {
		intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
		intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
		intentFilter
				.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
		intentFilter
				.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

		intentsClientService.addAction(DirectService.INTENT_PROGRESS_NOTIFY);
		intentsClientService.addAction(DirectService.INTENT_DISMISS_DIALOG);
		intentsClientService.addAction(DirectService.INTENT_FILE_PATH);
		intentsClientService.addAction(DirectService.INTENT_ERROR_IO_EXCEPTION);

	}

	/** register the BroadcastReceiver with the intent values to be matched */
	/*
	 * @Override public void onResume() { super.onResume(); callReceiver = new
	 * WiFiDirectReceiver(manager, channel, this);
	 * registerReceiver(callReceiver, intentFilter);
	 * LocalBroadcastManager.getInstance(this).registerReceiver(
	 * clientServiceListener, intentsClientService); }
	 */
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		receiver = new WiFiDirectReceiver(manager, channel, this);
		registerReceiver(receiver, intentFilter);
		LocalBroadcastManager.getInstance(this).registerReceiver(
				clientServiceListener, intentsClientService);
		Globals.isActivityAlive = true;

	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		unregisterReceiver(receiver);
		LocalBroadcastManager.getInstance(this).unregisterReceiver(
				clientServiceListener);
		Globals.isActivityAlive = false;
	}

	/*
	 * @Override public void onPause() { super.onPause();
	 * unregisterReceiver(callReceiver);
	 * LocalBroadcastManager.getInstance(this).unregisterReceiver(
	 * clientServiceListener); }
	 */

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		Log.v(TAG, "is resumed is set to false");
	}

	/**
	 * Remove all peers and clear all fields. This is called on
	 * BroadcastReceiver receiving a state change event.
	 */
	public void resetData() {
		// Utility.toast(getApplicationContext(), "Reset data");
		Log.v(TAG, "Resetting data");
		DeviceListFragment fragmentList = (DeviceListFragment) getFragmentManager()
				.findFragmentById(R.id.frag_list);
		DetailFragment fragmentDetails = (DetailFragment) getFragmentManager()
				.findFragmentById(R.id.frag_detail);
		if (fragmentList != null) {
			fragmentList.clearPeers();
			// fragmentList.resetViews();
		}
		if (fragmentDetails != null) {
			fragmentDetails.resetViews();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.action_items, menu);

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// This is for direccting user to wifi-direct settings. Change this
		// after adding wifi direct settings button
		/*
		 * case R.id.atn_direct_enable: if (manager != null && channel != null)
		 * {
		 * 
		 * // Since this is the system wireless settings activity, it's // not
		 * going to send us a result. We will be notified by //
		 * WiFiDeviceBroadcastReceiver instead.
		 * 
		 * startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS)); } else
		 * { Log.e(TAG, "channel or manager is null"); } return true;
		 */

		/*
		 * case R.id.atn_direct_discover: return onDiscoveryClicked(true);
		 */
		/*
		 * case R.id.test: // Remove this line later Log.v(TAG,
		 * "this device mac = "+Globals.thisDeviceAddress); IP.getIPAddress1();
		 * return true;
		 */
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public void onDiscoveryButtonClicked(View v) {
		if (new Preferences(this).getStatus().equalsIgnoreCase("C")) {
			startActivity(new Intent(this, ContactActivity.class));
			stopService(new Intent(this, DirectService.class));
			finish();
			return;
		}

		onDiscoveryClicked(true);
	}

	public void onEmulateCall(View v) {
		Intent intent = new Intent("emulatePhoneCall");
		intent.setClass(this, DirectService.class);
		LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

	}

	public boolean onDiscoveryClicked(final boolean showDialog) {
		Log.v(TAG, "refresh pressed");
		if (!isWifiP2pEnabled) {
			Toast.makeText(HomeActivity.this, R.string.p2p_off_warning,
					Toast.LENGTH_SHORT).show();
			startWifiDirect();

			return true;
		}
		final DeviceListFragment fragment = (DeviceListFragment) getFragmentManager()
				.findFragmentById(R.id.frag_list);
		if (showDialog || !isDialogShownOnce) {
			Log.v(TAG, "Progress bar shown");
			fragment.onInitiateDiscovery();
			isDialogShownOnce = true;
		}
		Log.v(TAG, "discovering peers ...");
		manager.discoverPeers(channel, new WifiP2pManager.ActionListener() {

			@Override
			public void onSuccess() {
				Log.v(TAG, "discovered peers, checking broadcast now");

				Log.v(TAG, "Discovery Initiated");
				// Utility.toast(HomeActivity.this,
				// "Discovery Initiated");
				discoveryCallCounter = 0;
			}

			@Override
			public void onFailure(int reasonCode) {
				Log.v(TAG, "Failed to discover Error code : " + reasonCode);
				if (reasonCode == WifiP2pManager.ERROR) {
					discoveryCallCounter++;
					Log.v(TAG, "tried " + discoveryCallCounter + " times");
					if (discoveryCallCounter < 20) {
						new AsyncDiscover().execute();
					} else {
						Log.v(TAG, "Give up. Do not do automatically");
						discoveryCallCounter = 0;

						Utility.toast(HomeActivity.this,
								"Discovery Failed, Please Restart Wifi Direct!!");
						restartWifiDirect();

					}
				} else if (reasonCode == WifiP2pManager.BUSY) {
					Toast.makeText(getApplicationContext(),
							"Wifi Direct is Busy, Please Wait or Restart Wifi",
							Toast.LENGTH_LONG).show();
					restartWifiDirect();
				}
			}

		});
		return true;
	}

	private void restartWifiDirect() {
		Builder builder = new AlertDialog.Builder(this);
		final int version = android.os.Build.VERSION.SDK_INT;

		CharSequence wifiOrDirect = version < JELLYBEAN ? "Wifi Direct?"
				: "WiFi";

		builder.setTitle("Restart " + wifiOrDirect);
		CharSequence message = wifiOrDirect
				+ " is Busy.Please Restart it to work properly.";
		builder.setMessage(message);
		builder.setPositiveButton("Restart", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (version < JELLYBEAN) {
					startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));

				} else {
					Utility.toast(HomeActivity.this, "Restarting WiFi");
					WifiManager manager = (WifiManager) getSystemService(WIFI_SERVICE);
					if (manager.isWifiEnabled()) {
						manager.setWifiEnabled(false);
						manager.setWifiEnabled(true);

					}
				}

			}
		});
		builder.setNegativeButton("No", null);
		builder.create();
		builder.show();

	}

	private void startWifiDirect() {
		Builder builder = new AlertDialog.Builder(this);
		final int version = android.os.Build.VERSION.SDK_INT;

		CharSequence wifiOrDirect = version < JELLYBEAN ? "Wifi Direct"
				: "WiFi";

		builder.setTitle("Enable " + wifiOrDirect + "?");
		CharSequence message = wifiOrDirect
				+ " should be started for  it to work.";
		builder.setMessage(message);
		builder.setPositiveButton("Start", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (version < JELLYBEAN) {
					startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));

				} else {
					Utility.toast(HomeActivity.this, "Starting WiFi");
					WifiManager manager = (WifiManager) getSystemService(WIFI_SERVICE);
					if (!manager.isWifiEnabled()) {
						manager.setWifiEnabled(true);

					}
				}

			}
		});
		builder.setNegativeButton("No", null);
		builder.create();
		builder.show();

	}

	@Override
	public void showDetails(WifiP2pDevice device) {
		Log.v(TAG, "mac =" + device.deviceAddress);
		DetailFragment fragment = (DetailFragment) getFragmentManager()
				.findFragmentById(R.id.frag_detail);
		fragment.onDeviceClick(device);

	}

	@Override
	public void connect(WifiP2pConfig config) {
		Globals.isInitiator = true;
		manager.connect(channel, config, new ActionListener() {

			@Override
			public void onSuccess() {
				// Utility.toast(getApplicationContext(),
				// "Connection Successful ");
				Log.v(TAG, "connection successful");
				// /Block incoming calls
				Intent intent = new Intent(
						DirectService.INTENT_ADD_BLOCKING_FEATURE);
				intent.setClass(HomeActivity.this, DirectService.class);
				LocalBroadcastManager.getInstance(HomeActivity.this)
						.sendBroadcast(intent);

				// WiFiDirectReceiver will notify us. Ignore for now.
			}

			@Override
			public void onFailure(int reason) {
				Globals.isInitiator = false;
				Log.v(TAG, "failed connection");
				Toast.makeText(HomeActivity.this,
						"Connect failed. Retry.", Toast.LENGTH_SHORT).show();
			}
		});
	}

	@Override
	public void disconnect() {
		Log.v(TAG, "inside disconnect");
		final DetailFragment fragment = (DetailFragment) getFragmentManager()
				.findFragmentById(R.id.frag_detail);
		fragment.resetViews();

		DeviceListFragment listFragment = (DeviceListFragment) getFragmentManager()
				.findFragmentById(R.id.frag_list);
		listFragment.resetViews();

		manager.removeGroup(channel, new ActionListener() {

			@Override
			public void onFailure(int reasonCode) {
				Log.d(TAG, "Disconnect failed. Reason :" + reasonCode);

			}

			@Override
			public void onSuccess() {
				fragment.getView().setVisibility(View.GONE);
			}

		});
	}

	@Override
	public void onChannelDisconnected() {
		// we will try once more
		if (manager != null && !retryChannel) {
			Toast.makeText(this, "Channel lost. Trying again",
					Toast.LENGTH_LONG).show();
			resetData();
			retryChannel = true;
			manager.initialize(this, getMainLooper(), this);
		} else {
			Toast.makeText(
					this,
					"Severe! Channel is probably lost premanently. Try Disable/Re-Enable P2P.",
					Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public void cancelDisconnect() {

		/*
		 * A cancel abort request by user. Disconnect i.e. removeGroup if
		 * already connected. Else, request WifiP2pManager to abort the ongoing
		 * request
		 */
		if (manager != null) {
			final DeviceListFragment fragment = (DeviceListFragment) getFragmentManager()
					.findFragmentById(R.id.frag_list);
			if (fragment.getDevice() == null
					|| fragment.getDevice().status == WifiP2pDevice.CONNECTED) {
				disconnect();
			} else if (fragment.getDevice().status == WifiP2pDevice.AVAILABLE
					|| fragment.getDevice().status == WifiP2pDevice.INVITED) {

				manager.cancelConnect(channel, new ActionListener() {

					@Override
					public void onSuccess() {
						Toast.makeText(HomeActivity.this,
								"Aborting connection", Toast.LENGTH_SHORT)
								.show();
					}

					@Override
					public void onFailure(int reasonCode) {
						Toast.makeText(
								HomeActivity.this,
								"Connect abort request failed. Reason Code: "
										+ reasonCode, Toast.LENGTH_SHORT)
								.show();
					}
				});
			}
		}

	}

	@Override
	public void notifier(double percent, String fileName, boolean isCompleted,
			boolean isDownloading, float speedMbps, String timeRemaining) {
		// TODO Auto-generated method stub
		// util.showNotification(notificationManager, mBuilder, percent,
		// fileName, isCompleted, isDownloading);

	}

	@Override
	public void showDialog(double percent, String fileName,
			boolean isCompleted, boolean isDownloading, float speedMbps) {
		Log.v(TAG, "show dialog");

	}

	@Override
	public void dismisDialog() {

	}

	@Override
	public void updateDialog(double percent, String fileName,
			boolean isCompleted, boolean isDownloading, float speedMbps) {

	}

	private class AsyncDiscover extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try {
				Log.v(TAG, "let me sleep for 2 sec before discovering");
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			onDiscoveryClicked(false);
		}

	}

	private class AsyncCheck extends AsyncTask<Void, Void, Void> {
		Preferences preferences;
		boolean stopService = false;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			preferences = new Preferences(HomeActivity.this);
			String status = preferences.getStatus();
			if (status.equalsIgnoreCase("A")) {

			} else if (status.equalsIgnoreCase("B")) {
				cancel(true);

			} else if (status.equalsIgnoreCase("C")) {
				stopService = true;

			}

		}

		@Override
		protected Void doInBackground(Void... params) {

			if (stopService)
				return null;

			Log.v(TAG, "Checking");
			JSONObject obj = new Ssh().getStatus();
			if (obj == null)
				return null;
			try {
				String status = obj.getString("status");
				if (status != null)
					preferences.setStatus(status);
				if (status == null || status.equalsIgnoreCase(""))
					return null;
				if (status.equalsIgnoreCase("C")) {
					stopService = true;

				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (stopService) {
				stopService(new Intent(HomeActivity.this,
						DirectService.class));
				Intent intent = new Intent(getBaseContext(),
						ContactActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				getApplication().startActivity(intent);

			}
		}

	}
}

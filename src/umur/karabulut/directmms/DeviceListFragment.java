package umur.karabulut.directmms;

import java.util.Collection;
import java.util.List;

import umur.karabulut.directmms.PeersInfo.Peer;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ListFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pManager.PeerListListener;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class DeviceListFragment extends ListFragment implements
		PeerListListener {

	private static final String TAG = Globals.TAG + "DeviceListFragment";
	// private List<WifiP2pDevice> peers = new ArrayList<WifiP2pDevice>();
	ProgressDialog progressDialog = null;
	ProgressDialog fileNotify = null;
	View mContentView = null;
	private WifiP2pDevice device;
	PeersInfo peersInfo = new PeersInfo();

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		// this.setListAdapter(new
		// WiFiPeerListAdapter(getActivity(),R.layout.row_devices, peers));
		this.setListAdapter(new WiFiPeerListAdapter1(peersInfo.getPeers()));
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mContentView = inflater.inflate(R.layout.device_list, null);
		TextView runningDevice = (TextView) mContentView
				.findViewById(R.id.runningDeviceName);
		runningDevice.setText(getResources().getString(R.string.label_device)
				+ '\n' + android.os.Build.MANUFACTURER + " - "
				+ android.os.Build.PRODUCT);

		return mContentView;
	}

	/**
	 * @return this device
	 */
	public WifiP2pDevice getDevice() {
		return device;
	}

	private static String getDeviceStatus(int deviceStatus) {
		Log.d(TAG, "Peer status :" + deviceStatus);
		switch (deviceStatus) {
		case WifiP2pDevice.AVAILABLE:
			return "Available";
		case WifiP2pDevice.INVITED:
			return "Invited";
		case WifiP2pDevice.CONNECTED:
			return "Connected";
		case WifiP2pDevice.UNAVAILABLE:
			return "Unavailable";
		case WifiP2pDevice.FAILED:
			return "Failed";
		default:
			Log.d(TAG, "Peer status : unknown");
			return "Unknown";

		}
	}

	/**
	 * Initiate a connection with the peer.
	 */
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		// WifiP2pDevice device = (WifiP2pDevice)
		// getListAdapter().getItem(position);

		Peer p = (Peer) getListAdapter().getItem(position);
		WifiP2pDevice device = p.getWifiP2pDevice();

		if (device.status != WifiP2pDevice.CONNECTED && (p.isAvailable()))
			((DeviceActionListener) getActivity()).showDetails(device);

	}

	/**
	 * Array adapter for DeviceListFragment that maintains WifiP2pDevice list.
	 */
	/*
	 * private class WiFiPeerListAdapterOld extends ArrayAdapter<WifiP2pDevice>
	 * {
	 * 
	 * private List<WifiP2pDevice> items;
	 *//**
	 * @param context
	 * @param textViewResourceId
	 * @param objects
	 */
	/*
	 * public WiFiPeerListAdapterOld(Context context, int textViewResourceId,
	 * List<WifiP2pDevice> objects) { super(context, textViewResourceId,
	 * objects); items = objects;
	 * 
	 * }
	 * 
	 * @Override public View getView(int position, View convertView, ViewGroup
	 * parent) { View v = convertView; if (v == null) { LayoutInflater vi =
	 * (LayoutInflater) getActivity()
	 * .getSystemService(Context.LAYOUT_INFLATER_SERVICE); v =
	 * vi.inflate(R.layout.row_devices, null); } WifiP2pDevice device =
	 * items.get(position); if (device != null) { TextView top = (TextView)
	 * v.findViewById(R.id.device_name); TextView bottom = (TextView) v
	 * .findViewById(R.id.device_details); if (top != null) {
	 * top.setText(device.deviceName); } if (bottom != null) {
	 * bottom.setText(getDeviceStatus(device.status)); }
	 * 
	 * if (device.status == WifiP2pDevice.CONNECTED) { ImageButton file_select =
	 * (ImageButton) v .findViewById(R.id.file_select_button);
	 * 
	 * ImageButton disconnect = (ImageButton) v
	 * .findViewById(R.id.disconnect_button);
	 * file_select.setImageResource(R.drawable.folder);
	 * disconnect.setImageResource(R.drawable.disconnect);
	 * file_select.setEnabled(true); disconnect.setEnabled(true); final String
	 * currentDeviceMac = device.deviceAddress;
	 * 
	 * file_select.setOnClickListener(new View.OnClickListener() {
	 * 
	 * @Override public void onClick(View v) {
	 * 
	 * Intent intentFileSelector = new Intent( Intent.ACTION_GET_CONTENT);
	 * intentFileSelector.setType("file/*");
	 * 
	 * // Utility.toast(getActivity(), // IP.getMACAddress("wlan0")); Log.v(TAG,
	 * "This ip address = " + IP.getIPAddress1()); Log.v(TAG, "mac address = " +
	 * Globals.thisDeviceAddress);
	 * 
	 * try {
	 * 
	 * Globals.justSelectedDeviceAddress = currentDeviceMac;
	 * 
	 * startActivityForResult(intentFileSelector, 20); } catch
	 * (ActivityNotFoundException e) { AlertDialog.Builder builder = new
	 * AlertDialog.Builder( getActivity());
	 * 
	 * builder.setMessage(R.string.file_manager_not_found_message);
	 * builder.setCancelable(true); builder.setPositiveButton("OK", new
	 * DialogInterface.OnClickListener() { public void onClick( DialogInterface
	 * dialog, int id) { Intent intent = new Intent( Intent.ACTION_VIEW);
	 * intent.setData(Uri .parse("market://search?q=file&nbsp;manager&c=apps"));
	 * startActivity(intent); } }); builder.setNegativeButton("Cancel", new
	 * DialogInterface.OnClickListener() { public void onClick( DialogInterface
	 * dialog, int id) { // User cancelled the dialog } });
	 * 
	 * AlertDialog dialog = builder.create(); builder.show(); }
	 * 
	 * } });
	 * 
	 * disconnect.setOnClickListener(new View.OnClickListener() {
	 * 
	 * @Override public void onClick(View v) { ((DeviceActionListener)
	 * getActivity()).disconnect(); } }); } else { ImageButton file_select =
	 * (ImageButton) v .findViewById(R.id.file_select_button); ImageButton
	 * disconnect = (ImageButton) v .findViewById(R.id.disconnect_button);
	 * file_select.setImageResource(R.drawable.folder_disabled);
	 * disconnect.setImageResource(R.drawable.disconnect_disabled);
	 * file_select.setEnabled(false); disconnect.setEnabled(false); } }
	 * 
	 * return v;
	 * 
	 * } }
	 */private class WiFiPeerListAdapter1 extends BaseAdapter {

		private List<Peer> items;

		public WiFiPeerListAdapter1(List<Peer> peerList) {
			this.items = peerList;

		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View v = convertView;
			if (v == null) {
				LayoutInflater vi = (LayoutInflater) getActivity()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.row_devices, null);
			}
			final Peer currentPeer = items.get(position);

			WifiP2pDevice device = currentPeer.getWifiP2pDevice();
			// String isAvailable = currentPeer.isAvailable() ? "OK" : "!";
			ImageView iv = (ImageView) v.findViewById(R.id.ivWarning);
			if (currentPeer.isAvailable()) {
				iv.setVisibility(View.INVISIBLE);
			} else {
				iv.setVisibility(View.VISIBLE);

			}
			boolean isDisconnected = currentPeer.isDisconnected();
			if (device != null) {
				TextView top = (TextView) v.findViewById(R.id.device_name);
				TextView bottom = (TextView) v
						.findViewById(R.id.device_details);
				if (top != null) {
					top.setText(device.deviceName);
				}
				if (bottom != null) {
					// bottom.setText(getDeviceStatus(device.status) + " " +
					// isAvailable);
					String status;
					if (isDisconnected) {
						status = "Disconnected";

					} else {
						status = getDeviceStatus(device.status);
						// bottom.setText(getDeviceStatus(device.status));
					}
					bottom.setText(status);

				}

				if (device.status == WifiP2pDevice.CONNECTED && !isDisconnected) {
					// ImageButton file_select = (ImageButton)
					// v.findViewById(R.id.file_select_button);

					Button disconnect = (Button) v
							.findViewById(R.id.disconnect_button);
					disconnect.setText("Disconnect");
					// file_select.setImageResource(R.drawable.folder);
					// file_select.setEnabled(true);
					disconnect.setEnabled(true);
					final String currentDeviceMac = device.deviceAddress;

					/*
					 * file_select.setOnClickListener(new View.OnClickListener()
					 * {
					 * 
					 * @Override public void onClick(View v) {
					 * 
					 * Intent intentFileSelector = new Intent(
					 * Intent.ACTION_GET_CONTENT);
					 * intentFileSelector.setType("file/*");
					 * 
					 * // Utility.toast(getActivity(), //
					 * IP.getMACAddress("wlan0")); Log.v(TAG,
					 * "This ip address = " + IP.getIPAddress1()); Log.v(TAG,
					 * "mac address = " + Globals.thisDeviceAddress);
					 * 
					 * try {
					 * 
					 * Globals.justSelectedDeviceAddress = currentDeviceMac;
					 * 
					 * startActivityForResult(intentFileSelector, 20); } catch
					 * (ActivityNotFoundException e) { AlertDialog.Builder
					 * builder = new AlertDialog.Builder( getActivity());
					 * 
					 * builder.setMessage(R.string.file_manager_not_found_message
					 * ); builder.setCancelable(true);
					 * builder.setPositiveButton("OK", new
					 * DialogInterface.OnClickListener() { public void onClick(
					 * DialogInterface dialog, int id) { Intent intent = new
					 * Intent( Intent.ACTION_VIEW); intent.setData(Uri
					 * .parse("market://search?q=file&nbsp;manager&c=apps"));
					 * startActivity(intent); } });
					 * builder.setNegativeButton("Cancel", new
					 * DialogInterface.OnClickListener() { public void onClick(
					 * DialogInterface dialog, int id) { // User cancelled the
					 * dialog } });
					 * 
					 * AlertDialog dialog = builder.create(); builder.show(); }
					 * 
					 * } });
					 */
					disconnect.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							((DeviceActionListener) getActivity()).disconnect();

							AlertDialog.Builder b = new Builder(getActivity());
							b.setTitle("Disconnect?");
							b.setMessage("Connection will be lost ");
							b.setNegativeButton("NO", null);
							b.setPositiveButton("OK", new OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {

									peersInfo.getPeers().get(position)
											.setDisconnected(true);

								}
							});

							b.show();
						}
					});
				} else {

					Button disconnect = (Button) v
							.findViewById(R.id.disconnect_button);

					disconnect.setEnabled(false);
					disconnect.setVisibility(View.INVISIBLE);
				}
			}

			return v;

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return items.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return items.get(arg0);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		DetailFragment fragment = (DetailFragment) getFragmentManager()
				.findFragmentById(R.id.frag_detail);

		fragment.startFileTransfer(requestCode, resultCode, data);
	}

	/**
	 * Update UI for this device.
	 * 
	 * @param device
	 *            WifiP2pDevice object
	 */
	public void updateThisDevice(WifiP2pDevice device) {

		this.device = device;
		TextView view = (TextView) mContentView.findViewById(R.id.my_name);
		view.setText(device.deviceName);
		Globals.thisDeviceAddress = device.deviceAddress;

	}

	@Override
	public void onPeersAvailable(WifiP2pDeviceList peerList) {
		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
		}
		Log.v(TAG, "no of peers = " + peerList.getDeviceList().size());
		// Log.v(TAG, "removing the peers");
		// peers.clear();
		Log.v(TAG, "showing all peers in the list now");
		// peers.addAll(peerList.getDeviceList());
		peersInfo.addPeers(peerList.getDeviceList());
		setListAdapter(new WiFiPeerListAdapter1(peersInfo.getPeers()));
		((WiFiPeerListAdapter1) getListAdapter()).notifyDataSetChanged();
		this.getListView().invalidate();
		// if (peers.size() == 0) {
		if (peersInfo.getPeers().size() == 0) {
			Log.d(TAG, "No devices found");
			return;
		}

	}

	public void clearPeers() {
		// peers.clear();
		peersInfo.clearPeers();
		((WiFiPeerListAdapter1) getListAdapter()).notifyDataSetChanged();
	}

	/**
     * 
     */
	public void onInitiateDiscovery() {
		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
		}
		progressDialog = ProgressDialog.show(getActivity(), null,
				"Searching for peers", true, true,
				new DialogInterface.OnCancelListener() {

					@Override
					public void onCancel(DialogInterface dialog) {

					}
				});
	}

	public ProgressDialog duringFileTransfer() {
		if (fileNotify != null && fileNotify.isShowing()) {
			fileNotify.dismiss();
		}

		/*
		 * //Dialog EOT addition
		 * fileNotify.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		 * fileNotify.setProgress(0);
		 */

		fileNotify = ProgressDialog.show(this.getActivity(),
				getString(R.string.file_transfer_notification_label),
				getString(R.string.file_transfer_notification_message), false);

		return fileNotify;

	}

	public void stopFileNotification() {
		if (fileNotify != null && fileNotify.isShowing()) {
			fileNotify.dismiss();
			Toast.makeText(getActivity(), "file notification stopped",
					Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * An interface-callback for the activity to listen to fragment interaction
	 * events.
	 */
	public interface DeviceActionListener {

		void showDetails(WifiP2pDevice device);

		void cancelDisconnect();

		void connect(WifiP2pConfig config);

		void disconnect();
	}

	public void resetViews() {
		View v = getView();
		// ImageButton file_select = (ImageButton)
		// v.findViewById(R.id.file_select_button);
		Button disconnect = (Button) v.findViewById(R.id.disconnect_button);
		// file_select.setImageResource(R.drawable.folder_disabled);
		// file_select.setEnabled(false);
		disconnect.setEnabled(false);
		disconnect.setVisibility(View.INVISIBLE);
	}

	public void updatePeers(Collection<WifiP2pDevice> peers2) {
		Log.v(TAG, "before size=" + peersInfo.getPeers().size());
		peersInfo.addPeers(peers2);
		Log.v(TAG, "after size = " + peersInfo.getPeers().size());
		this.setListAdapter(new WiFiPeerListAdapter1(peersInfo.getPeers()));
		((WiFiPeerListAdapter1) getListAdapter()).notifyDataSetChanged();
		getListView().invalidate();
	}

}

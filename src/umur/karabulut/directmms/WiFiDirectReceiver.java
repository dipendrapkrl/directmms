/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package umur.karabulut.directmms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.net.wifi.p2p.WifiP2pManager.PeerListListener;
import android.util.Log;

/**
 * A BroadcastReceiver that notifies of important wifi p2p events.
 */
public class WiFiDirectReceiver extends BroadcastReceiver {
	private static final String TAG = Globals.TAG + "WiFiDirectReceiver";

	private WifiP2pManager manager;
	private Channel channel;
	private HomeActivity activity;

	/**
	 * @param manager
	 *            WifiP2pManager system service
	 * @param channel
	 *            Wifi p2p channel
	 * @param activity
	 *            activity associated with the callReceiver
	 */
	public WiFiDirectReceiver(WifiP2pManager manager, Channel channel,
			HomeActivity activity) {
		super();
		this.manager = manager;
		this.channel = channel;
		this.activity = activity;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.content.BroadcastReceiver#onReceive(android.content.Context,
	 * android.content.Intent)
	 */
	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
			Log.d(TAG, "Track: WDB - or");
			Log.v(TAG, "intent received");
			// UI update to indicate wifi p2p status.
			int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
			if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
				Log.v(TAG, "wifi direct is enabled");
				// Wifi Direct mode is enabled
				activity.setIsWifiP2pEnabled(true);
				activity.onDiscoveryClicked(false);

				// Utility.toast(context, "enabled");
			} else {
				// Utility.toast(context, "not enabled");
				Log.v(TAG, "wifi direct is not enabled");
				activity.setIsWifiP2pEnabled(false);

				activity.resetData();

			}
			Log.d(TAG, "P2P state changed - " + state);

		} else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {
			Log.v(TAG, "peers changed broadcast received. finding  devices..");
			// Utility.toast(activity, "Peers changed");

			// request available peers from the wifi p2p manager. This is an
			// asynchronous call and the calling activity is notified with a
			// callback on PeerListListener.onPeersAvailable()
			Log.d(TAG, "P2P peers changed");
			if (manager != null) {
				manager.requestPeers(channel, (PeerListListener) activity
						.getFragmentManager().findFragmentById(R.id.frag_list));
			}
		} else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION
				.equals(action)) {

			Log.v(TAG, "Connection changed");
			// Utility.toast(activity, "Connection changed");

			if (manager == null) {
				Log.v(TAG, "manager is null");
				return;
			}

			NetworkInfo networkInfo = (NetworkInfo) intent
					.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);

			if (networkInfo.isConnected()) {

				Log.v(TAG,
						"device is connected  , Now request connection information");
				// we are connected with the other device, request connection
				// info to find group owner IP

				DetailFragment fragment = (DetailFragment) activity
						.getFragmentManager()
						.findFragmentById(R.id.frag_detail);
				// Utility.toast(activity, "request connection info");
				manager.requestConnectionInfo(channel, fragment);
			} else {
				//
				// Toast.makeText(activity, "Disconnected", Toast.LENGTH_LONG)
				// .show();
				// activity.stopService(new Intent(activity,
				// DirectService.class));
				// activity.startService(new Intent(activity,
				// DirectService.class));

				// It's a disconnect
				Log.v(TAG, "disconnected");
				/*
				 * DeviceListFragment fragment = (DeviceListFragment) activity
				 * .getFragmentManager().findFragmentById(R.id.frag_list);
				 */// Add empty peers to the list
				/*
				 * Collection<WifiP2pDevice> peers = new
				 * ArrayList<WifiP2pDevice>(); Log.v(TAG, "add ! to the peers");
				 * fragment.updatePeers(peers);
				 */
				// activity.resetData();
			}
		} else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION
				.equals(action)) {
			// Utility.toast(activity, "Details of this device is changed");
			Log.v(TAG, "Detail of this device is changed");
			DeviceListFragment fragment = (DeviceListFragment) activity
					.getFragmentManager().findFragmentById(R.id.frag_list);
			fragment.updateThisDevice((WifiP2pDevice) intent
					.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_DEVICE));

		}
	}
}

package umur.karabulut.directmms;

import java.io.File;
import java.net.URISyntaxException;

import umur.karabulut.directmms.DeviceListFragment.DeviceActionListener;
import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager.ConnectionInfoListener;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;

/**
 * A fragment that manages a particular peer and allows interaction with device
 * i.e. setting up network connection and transferring data.
 */
public class DetailFragment extends Fragment implements ConnectionInfoListener {

	private static final String TAG = Globals.TAG + "DetailFragment";

	public static final String IP_SERVER = "192.168.49.1";
	public static int PORT = 8989;
	// private static boolean server_running = false;

	protected static final int CHOOSE_FILE_RESULT_CODE = 20;
	private View mContentView = null;
	private WifiP2pDevice device;
	ProgressDialog progressDialog = null;

	private static String fileName;
	private static String fileMimeType;
	private static DeviceListFragment fragment;

	private Intent intentFileSelector;

	private static Notifier notifier;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mContentView = inflater.inflate(R.layout.device_detail, null);
		mContentView.findViewById(R.id.btn_connect).setOnClickListener(
				new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						WifiP2pConfig config = new WifiP2pConfig();

						config.deviceAddress = device.deviceAddress;

						// New fix trying to solve the group owner-client
						// redundancy. Remove if it makes the app unstable
						config.groupOwnerIntent = 0;

						Log.d(TAG, "Device detail main -> " + device.toString());

						config.wps.setup = WpsInfo.PBC;
						if (progressDialog != null
								&& progressDialog.isShowing()) {
							progressDialog.dismiss();
						}
						progressDialog = ProgressDialog.show(getActivity(),
								null, "Connecting to " + device.deviceAddress,
								true, true);
						((DeviceActionListener) getActivity()).connect(config);

					}
				});

		mContentView.findViewById(R.id.btn_disconnect).setOnClickListener(
				new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						((DeviceActionListener) getActivity()).disconnect();
					}
				});

		mContentView.findViewById(R.id.btn_start_client).setOnClickListener(
				new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						Log.d(TAG, "FOlder button clicked ");
						intentFileSelector = new Intent(
								Intent.ACTION_GET_CONTENT);
						intentFileSelector.setType("file/*");

						startActivityForResult(intentFileSelector,
								CHOOSE_FILE_RESULT_CODE);

					}
				});

		fragment = (DeviceListFragment) getFragmentManager().findFragmentById(
				R.id.frag_list);

		return mContentView;
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);

		try {
			notifier = (Notifier) activity;

		} catch (ClassCastException e) {
			Log.v(TAG, "activity should implement notifier interface");
			e.printStackTrace();
		}
	}

	public void startFileTransfer(int requestCode, int resultCode, Intent data) {
		// To check if any file was selected. RESULT_OK == -1
		if (resultCode == Activity.RESULT_OK) {

			// String localIP = Utility.getLocalIPAddress();
			String localIP = Utility.getIPAddress();// Utility.getLocalIpv4Address();
			// Trick to find the ip in the file /proc/net/arp
			Log.d(TAG, "LOCAL IP -> " + localIP);

			// Log.d(HomeActivity.TAG, "CLIENT IP -> "+clientIP);
			Log.d(TAG, "IP SERVER -> " + IP_SERVER);

			// User has picked a file. Transfer it to group owner i.e peer using
			// DirectService.
			Uri uri = data.getData();

			Log.d(TAG, "Intent----------- " + uri);
			String path = "";
			try {
				path = getPath(getActivity(), uri);
				Log.d(TAG, "file path = " + path);
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Log.v(TAG, "Couldnt retrieve file path, sorry");
			}

			// File f = new File("" + uri);
			File f = new File(path);
			fileName = f.getName();
			Log.d(TAG, "File name----------- " + fileName);

			/*
			 * String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
			 * .toString()); String fileMimeType = MimeTypeMap.getSingleton()
			 * .getMimeTypeFromExtension(fileExtension);
			 */
			String fileExtension = MimeTypeMap.getFileExtensionFromUrl(path);
			String fileMimeType = MimeTypeMap.getSingleton()
					.getMimeTypeFromExtension(fileExtension);
			long size = f.length();

			if (fileMimeType == null)
				fileMimeType = "*/*";

			Log.d(TAG, "File type ------ " + fileExtension);
			Log.d(TAG, "File mime ------ " + fileMimeType);

			Intent serviceIntent = new Intent(getActivity(),
					DirectService.class);
			serviceIntent.setAction(DirectService.INTENT_START_CLIENT);
			serviceIntent.putExtra(DirectService.EXTRAS_FILE_PATH,
					uri.toString());
			serviceIntent.putExtra(DirectService.FILE_NAME, fileName);
			serviceIntent.putExtra(DirectService.FILE_SIZE, size);
			serviceIntent.putExtra(DirectService.FILE_MIME_TYPE, fileMimeType);

			if (localIP.equals(IP_SERVER)) {
				// if()
				// serviceIntent.putExtra(DirectService.EXTRAS_ADDRESS,
				// clientIP);x
				// Log.d(HomeActivity.TAG,
				// "Case 1: Sending to IP -> "+clientIP);
				Log.v(TAG, "local ip is equal to server ip");
				/*
				 * Utility.toast(getActivity(), "local ip is equals server ip "
				 * + localIP);
				 */
				Log.v(TAG, "just selected device mac="
						+ Globals.justSelectedDeviceAddress);
				String ip = new Preferences(getActivity())
						.getIPFromMac(Globals.justSelectedDeviceAddress);
				// MacIpPair.getIPFromMac(Globals.justSelectedDeviceAddress);
				Log.v(TAG, "its ip = " + ip);

				// if (ip != null)
				serviceIntent.putExtra(DirectService.EXTRAS_ADDRESS, ip);
				Log.v(TAG, "will try to send to " + ip);

				if (ip.equals("")) {
					Utility.toast(getActivity(),
							"Please re-connect to send the file");
					return;
				}
			} else {
				// Utility.toast(getActivity(), "IP Address=" + localIP);
				serviceIntent.putExtra(DirectService.EXTRAS_ADDRESS, IP_SERVER);
				Log.d(TAG, "Case 2: Sending to IP -> " + IP_SERVER);
			}

			serviceIntent.putExtra(DirectService.EXTRAS_PORT, PORT);
			LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(
					serviceIntent);
			// getActivity().startService(serviceIntent);
		}

	}

	public static String getPath(Context context, Uri uri)
			throws URISyntaxException {
		if ("content".equalsIgnoreCase(uri.getScheme())) {
			String[] projection = { "_data" };
			Cursor cursor = null;

			try {
				cursor = context.getContentResolver().query(uri, projection,
						null, null, null);
				int column_index = cursor.getColumnIndexOrThrow("_data");
				if (cursor.moveToFirst()) {
					return cursor.getString(column_index);
				}
			} catch (Exception e) {
				// Eat it
			}
		}

		else if ("file".equalsIgnoreCase(uri.getScheme())) {
			return uri.getPath();
		}

		return null;
	}

	@Override
	public void onConnectionInfoAvailable(final WifiP2pInfo info) {
		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
		}

		// if (!server_running) {
		startServer();
		if (!info.isGroupOwner) {
			startClientMeta();
			Globals.connectedDeviceIPAddress = IP_SERVER;
		}

		// new ServerAsyncTask(getActivity()).execute();

		// Log.v(TAG, "Server is running(AsyncTask)");
		// server_running = true;
		// }

		// hide the connect button. this from old ui. dont change until design
		// overhaul
		mContentView.findViewById(R.id.btn_connect).setVisibility(View.GONE);
	}

	private void startClientMeta() {
		// Send info if ip is changed else no need to send ip and mac
		// information
		Log.v(TAG, "Determine whether to send info to server or not");
		if (Globals.thisDeviceIP == null
				|| (!Utility.getIPAddress().equals(Globals.thisDeviceIP))) {

			Log.v(TAG, "ip is changed, so send to server");
			// Globals.thisDeviceIP = Utility.getLocalIpv4Address();
			Globals.thisDeviceIP = Utility.getIPAddress();

			Intent intent = new Intent(DirectService.INTENT_SEND_TO_SERVER_META);
			intent.putExtra(DirectService.INTENT_EXTRAS_IP_ADDRESS,
					Globals.thisDeviceIP);
			intent.putExtra(DirectService.EXTRAS_ADDRESS,
					DetailFragment.IP_SERVER);
			intent.putExtra(DirectService.EXTRAS_PORT, DetailFragment.PORT);

			// intent.putExtra(ClientService.INTENT_EXTRAS_MAC_ADDRESS,Utility.getMacAddress(HomeActivity.this));
			intent.putExtra(DirectService.INTENT_EXTRAS_MAC_ADDRESS,
					Globals.thisDeviceAddress);
			intent.setClass(getActivity(), DirectService.class);
			LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(
					intent);
		}

	}

	private void startServer() {
		Intent intent = new Intent(DirectService.INTENT_START_SERVER);
		intent.setClass(getActivity(), DirectService.class);

		// Log.v(TAG, "connection info available " +
		// Utility.getLocalIpv4Address());
		Log.v(TAG, "connection info available " + Utility.getIPAddress());

		LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
	}

	/**
	 * Updates the UI with device data
	 * 
	 * @param device
	 *            the device to be displayed
	 */
	public void showDetails(WifiP2pDevice device) {
		this.device = device;
	}

	/**
	 * Clears the UI fields after a disconnect or direct mode disable operation.
	 */
	public void resetViews() {

	}

	public void onDeviceClick(WifiP2pDevice device) {
		WifiP2pConfig config = new WifiP2pConfig();

		config.deviceAddress = device.deviceAddress;

		config.wps.setup = WpsInfo.PBC;
		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
		}
		progressDialog = ProgressDialog.show(getActivity(), null,
				"Connecting to " + device.deviceName, true, true);
		((DeviceActionListener) getActivity()).connect(config);

	}

	public interface Notifier {
		public void notifier(double percent, String fileName,
				boolean isCompleted, boolean isDownloading, float speedMbps,
				String timeRemaining);

	}

	public interface ProgressBehaviourListener {
		public void showDialog(double percent, String fileName,
				boolean isCompleted, boolean isDownloading, float speedMpbs);

		public void dismisDialog();

		public void updateDialog(double percent, String fileName,
				boolean isCompleted, boolean isDownloading, float speedMpbs);

	}

}

package umur.karabulut.directmms;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RecordingActivity extends Activity {

	private static final String TAG = "SoundRecordingActivity";
	private MediaRecorder recorder;
	private File audiofile = null;
	private String fileName = null;
	private String phoneNumber = "UnknownNumber";
	private boolean isRecording = false;
	private TextView tvNumber;

	public static final String INTENT_RECORDED_AUDIO_PATH = "FILE_PATH";
	public static final String INTENT_EXTRAS_AUDIO_LOCATION = "FILE_LOCATION";
	public static final String INTENT_EXTRAS_MESSAGE = "MESSAGE";
	public static final String INTENT_EXTRAS_PHONE_NUMBER = "PHONE_NUMBER";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_recording);
		tvNumber = (TextView) findViewById(R.id.tvNumber);

		String phNo = getIntent().getStringExtra("phoneNumber");
		if (phNo != null)

			phoneNumber = phNo;
		tvNumber.setText(phoneNumber);

		// startService(new Intent(this, CallListenerService.class));

	}

	public void clickHandler(View v) {
		switch (v.getId()) {
		case R.id.btnRecorder:

			if (!isRecording) {
				((Button) findViewById(R.id.btnRecorder)).setText("Stop");
				isRecording = true;
				try {
					startRecording();
				} catch (Exception e) {
					Log.v(TAG, "Error in recording");
					Toast.makeText(this, "Error in Recording",
							Toast.LENGTH_LONG).show();
					e.printStackTrace();
					isRecording = false;
					((Button) findViewById(R.id.btnRecorder)).setText("Record");
				}
			} else {
				((Button) findViewById(R.id.btnRecorder))
						.setText("Record Again");
				isRecording = false;
				stopRecording();
				((Button) findViewById(R.id.btnSend)).setEnabled(true);

			}
			break;
		case R.id.btnSend:
			if (fileName == null) {
				Toast.makeText(this, "Please record message before sending",
						Toast.LENGTH_LONG).show();
				return;
			}
			String msg = ((EditText) findViewById(R.id.etMessage)).getText()
					.toString();
			Intent intent = new Intent(INTENT_RECORDED_AUDIO_PATH);
			Log.v(TAG, "ph=" + phoneNumber);
			Log.v(TAG, "msg=" + msg);
			intent.putExtra(INTENT_EXTRAS_AUDIO_LOCATION, fileName);
			intent.putExtra(INTENT_EXTRAS_PHONE_NUMBER, phoneNumber);
			intent.putExtra(INTENT_EXTRAS_MESSAGE, (msg != null) ? msg : "");
			intent.setClass(this, DirectService.class);
			LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

			/*
			 * Intent intent1 = new Intent(
			 * CallListenerService.INTENT_ADD_BLOCKING_FEATURE);
			 * intent1.setClass(this, CallListenerService.class);
			 * LocalBroadcastManager.getInstance(this).sendBroadcast(intent1);
			 */
			finish();
			break;
		default:
			Log.v(TAG, "not defined");

		}

	}

	public void startRecording() throws Exception {

		fileName = Environment.getExternalStorageDirectory()
				+ "/DirectMms/Recorded/";
		fileName = fileName
				+ new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US)
						.format(new Date()) + "__" + phoneNumber + ".3gp";

		audiofile = new File(fileName);
		File dirs = new File(audiofile.getParent());

		if (!dirs.exists())
			dirs.mkdirs();

		if (audiofile.exists()) {
			audiofile.delete();
		}

		// audiofile = File.createTempFile("sound", ".3gp", sampleDir);

		recorder = new MediaRecorder();
		recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
		recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
		recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
		recorder.setOutputFile(audiofile.getAbsolutePath());
		recorder.prepare();
		recorder.start();

	}

	public void stopRecording() {

		recorder.stop();
		recorder.release();
		Toast.makeText(this, "File is saved to " + fileName, Toast.LENGTH_LONG)
				.show();
		// addRecordingToMediaLibrary();
	}

	protected void addRecordingToMediaLibrary() {
		ContentValues values = new ContentValues(4);
		long current = System.currentTimeMillis();
		values.put(MediaColumns.TITLE, "audio" + audiofile.getName());
		values.put(MediaColumns.DATE_ADDED, (int) (current / 1000));
		values.put(MediaColumns.MIME_TYPE, "audio/3gpp");
		values.put(MediaColumns.DATA, audiofile.getAbsolutePath());
		ContentResolver contentResolver = getContentResolver();

		Uri base = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
		Uri newUri = contentResolver.insert(base, values);

		sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, newUri));
		Toast.makeText(this, "Added File " + newUri, Toast.LENGTH_LONG).show();
	}
}
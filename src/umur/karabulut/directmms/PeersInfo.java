package umur.karabulut.directmms;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.net.wifi.p2p.WifiP2pDevice;
import android.util.Log;

public class PeersInfo {
	private static final String TAG = Globals.TAG + "PeersInfo";

	private int id = 0;

	private List<Peer> peersList = new ArrayList<PeersInfo.Peer>();

	/**
	 * 
	 * @param peers
	 *            add these pairs to the existing list of pairs
	 */
	public void addPeers(Collection<WifiP2pDevice> peers) {
		List<Peer> tmpList = new ArrayList<PeersInfo.Peer>();
		for (WifiP2pDevice wifiP2pDevice : peers) {
			tmpList.add(new Peer(id, wifiP2pDevice));

			if (!ifPresentUpdate(wifiP2pDevice)) {
				peersList.add(new Peer(id, wifiP2pDevice));
				id++;
			}
		}

		updateAvailability(tmpList);
		unsetDisconnected();

	}

	private void unsetDisconnected() {
		for (int i = 0; i < peersList.size(); i++) {
			peersList.get(i).setDisconnected(false);
		}
	}

	/**
	 * updates the availability of the peers which were found earlier but not
	 * now
	 * 
	 * @param tmpList
	 *            current list of pairs
	 */
	private void updateAvailability(List<Peer> tmpList) {
		List<Integer> indices = new ArrayList<Integer>();
		Integer location = 0;
		for (Peer peer : peersList) {
			boolean isFound = false;
			for (Peer peerTmp : tmpList) {
				if (peer.getWifiP2pDevice().deviceAddress
						.equalsIgnoreCase(peerTmp.getWifiP2pDevice().deviceAddress)) {
					isFound = true;
					break;

				}
			}
			if (!isFound) {
				indices.add(location);
			}

			location = location + 1;
		}

		// Update the availability to false since they are not currently
		// available
		for (Integer integer : indices) {
			peersList.get(integer).setAvailability(false);
			Log.v(TAG,
					"available= false for device with name="
							+ peersList.get(integer).getWifiP2pDevice().deviceName);

		}
	}

	/**
	 * updates the device if present and returns true else returns false;
	 * 
	 * @param wifiP2pDevice
	 * @return true if finds and updates else false;
	 */
	private boolean ifPresentUpdate(WifiP2pDevice wifiP2pDevice) {
		int location = 0;
		boolean isPresent = false;
		for (Peer peer : peersList) {
			if (peer.getWifiP2pDevice().deviceAddress
					.equalsIgnoreCase(wifiP2pDevice.deviceAddress)) {
				isPresent = true;
				break;
			}
			location++;

		}
		if (isPresent) {
			peersList.get(location).updateWifiP2pDevice(wifiP2pDevice);
			peersList.get(location).setAvailability(true);
			return true;
		}

		return false;
	}

	public List<Peer> getPeers() {
		Collections.sort(peersList, new CustomComparator());
		return peersList;

	}

	public void clearPeers() {
		peersList = new ArrayList<PeersInfo.Peer>();
		id = 0;

	}

	private class CustomComparator implements Comparator<Peer> {

		@Override
		public int compare(Peer lhs, Peer rhs) {

			return lhs.getId() - rhs.getId();
		}

	}

	public class Peer {
		private int id;
		private boolean isAvailable = false;
		private boolean isDisconnected = false;
		private WifiP2pDevice wifiP2pDevice;

		public Peer(int id, WifiP2pDevice device) {
			this.id = id;
			this.isAvailable = true;
			this.wifiP2pDevice = device;

		}

		public int getId() {
			return id;
		}

		public boolean isAvailable() {
			return isAvailable;
		}

		public boolean isDisconnected() {
			return isDisconnected;
		}

		public void setDisconnected(boolean isDisconnected) {
			this.isDisconnected = isDisconnected;
		}

		public WifiP2pDevice getWifiP2pDevice() {
			return wifiP2pDevice;
		}

		public void setAvailability(boolean isAvailable) {
			this.isAvailable = isAvailable;
		}

		public void updateWifiP2pDevice(WifiP2pDevice device) {
			this.wifiP2pDevice = device;
		}

	}

}
